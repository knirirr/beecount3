BeeCount 3
==========

Why?
----

One of the most frequent user requests for [BeeCount version 2](https://github.com/knirirr/BeeCount) is some sort of
cloud sync so that users can count the same projects on muliple devices. To do this with the Android version would
require quite a bit of work both modifying the application and creating a back end. There's also some interest from
iOS users (often those who've defected from Android) in using BeeCount, but there's not enough interest to cover the
cost of Apple's developer fees.

So, the obvious solution is a web application, for which Meteor would seem like a good choice.

Why not?
--------

Unfortunately a web app won't start up if used in an area with no signal. It might be possible to make a Cordova app
with local data storage, but if that can be done it would probably be for Android only.

Purpose
-------

The purpose of BeeCount is to flip out and assist in the counting of knitting and crochet projects, or similar. It has
been designed to allow considerable flexibility by means of defining multiple counts and linking them together. Of course,
 it is not limited to counting only knitting; an example of another use is provided by the
[TransektCount](https://github.com/wistein/TransektCount) application, an adaptation of BeeCount for counting
butterflies.

Using BeeCount 3
----------------

At the time of writing an single instance of BeeCount 3 is running at
[beecountapp.knirirr.com](https://beecountapp.knirirr.com). Or, as a Meteor application you could run it yourself
on a server with Meteor installed.


Licence
-------

This code is licensed under the simplified BSD license (see LICENSE.txt).



