T9n = (require 'meteor-accounts-t9n').T9n;
T9n.map 'en', require 'meteor-accounts-t9n/build/en'
Template.registerHelper 't9n', (x, params) -> T9n.get(x, true, params.hash);

