import { Template } from 'meteor/templating'
import { Autorun } from 'meteor/templating'

import '../imports/api/counts.js'
import '../imports/api/projects.js'
import '../imports/api/alerts.js'
import '../imports/api/links.js'
import '../imports/api/auth.js'

import '../imports/layouts/layout.html'
import '../imports/layouts/layout.js'
import '../imports/layouts/loading.html'

import '../imports/views/welcome.html'
import '../imports/views/instructions.html'
import '../imports/views/instructions.js'
import '../imports/views/privacy.html'
import '../imports/views/privacy.js'
import '../imports/views/new_project.js'
import '../imports/views/new_project.html'
import '../imports/views/list_projects.js'
import '../imports/views/list_projects.html'
import '../imports/views/counting.js'
import '../imports/views/counting.html'
import '../imports/views/edit_project.js'
import '../imports/views/edit_project.html'
import '../imports/views/edit_count.js'
import '../imports/views/edit_count.html'
import '../imports/views/account.js'
import '../imports/views/account.html'


Meteor.startup(() => {
    ClientStorage = require('ClientStorage').ClientStorage;
    /*
    const starting = FlowRouter.getRouteName();
    console.log('client starting at ' + starting);
    const location = ClientStorage.get('beecount_location');
    if ((location.indexOf('account') !== -1) ||  (location.indexOf('reset') !== -1)) {
        console.log("Not redirecting an account URL.");
        return;
    }
    if (location) {
        var re = new RegExp(starting, 'gi');
        var arr = location.match(re) || [];
        if (arr.length > 0) {
            console.log('already here!');
        } else {
            console.log('redirecting to ' + location);
            FlowRouter.go(location);
        }
    } else {
        console.log('no stored session found');
    }
    // Start the service worker from https://github.com/NitroBAY/meteor-service-worker
    navigator.serviceWorker.register('/sw.js').then(
        console.log('ServiceWorker registered')
    ).catch(
        error => console.log('ServiceWorker registration failed: ', err)
    );
    */
});

Tracker.autorun(function () {
    var stat;
    if (Meteor.status().status === "connected") {
        stat = 'green';
        text = 'connected';
    }
    else if (Meteor.status().status === "connecting") {
        stat = 'yellow darken-2';
        text = 'connecting';
    }
    else {
        stat = 'red';
        text = 'disconnected';
    }
    Session.set('status',stat);
    Session.set('statustext',text);
});
