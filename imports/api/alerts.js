/**
 * Created by milo on 05/04/2017.
 */

import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import SimpleSchema from 'simpl-schema';

import './counts.js'

Alerts = new Meteor.Collection('alerts');
AlertSchema = new SimpleSchema({
    alert: {
        type: Number,
        label: 'Alert',
        min: 1
    },
    message: {
        type: String,
        label: 'Message'
    },
    knitter: {
        type: String,
        label: 'Knitter',
    },
});
Alerts.attachSchema(AlertSchema);

/*
if (Meteor.isCordova) {
    Ground.Collection(Links);
}
*/

Meteor.methods({
    'alerts.remove'(alertId) {
        check(alertId,String);
        Alerts.remove(alertId);
    },
    'alerts.delete-from-all'(alertId,countId) {
        check(alertId,String);
        check(countId,String);
        const projectId = Projects.findOne({},{counts: countId, alerts: alertId});
        Projects.update(projectId,{$pull: {alerts: alertId}});
        Counts.update(countId,{$pull: {alerts: alertId}});
        Alerts.remove({_id: alertId});
    }
})

/*
if (Meteor.isCordova) {
    if ( Meteor.isClient ) {
        Ground.methodResume([
            'remove',
            'delete-from-all'
        ]);
    }
}
*/
