import { Meteor } from 'meteor/meteor';

import './alerts.js'
import './counts.js'
import './links.js'
import './projects.js'

Meteor.methods({
    'auth.is_admin'() {

        if (! Meteor.userId()) {
            throw new Meteor.Error('not-authorized', "Please log in!");
        }
        const user = Meteor.users.findOne(Meteor.userId());
        const address = user.emails[0].address;
        /*
         * Well, no-one else should be able to sign up with this address...
         */
        if (address === Meteor.settings.admin_email) {
            return true;
        }
        return false;
    },
    'auth.remove_user'(id) {
        check(id, String);
        if (! Meteor.userId()) {
            throw new Meteor.Error('not-authorized', "Please log in!");
        }
        if ((Meteor.userId() === id) || (Meteor.call('auth.is_admin'))) {
            // Delete the account, having first deleted all associated records.
            Projects.find({knitter: id}).forEach(function(project) {
                console.log("Removing project: " + project._id);
                Projects.remove(project._id);
            });
            Counts.find({knitter: id}).forEach(function(count) {
                console.log("Removing count: " + count._id);
                Counts.remove(count._id);
            });
            Links.find({knitter: id}).forEach(function(link) {
                console.log("Removing link: " + link._id);
                Links.remove(link._id);
            });
            Alerts.find({knitter: id}).forEach(function(alert) {
                console.log("Removing alert: " + alert._id);
                Alerts.remove(alert._id);
            });
            console.log("Would remove user: " + id);
            Meteor.users.remove(id);
        } else {
            throw new Meteor.Error('not-authorized', "Deletion prohibited!");
        }
    }
});
