/**
 * Created by milo on 31/01/2017.
 */
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import SimpleSchema from 'simpl-schema';

import './alerts.js';

Counts = new Meteor.Collection('counts');
CountSchema = new SimpleSchema({
    name: {
        type: String,
        label: 'Name'
    },
    count: {
        type: Number,
        label: 'Count',
        min: 0
    },
    auto_reset: {
        type: Number,
        label: 'Auto Reset',
        min: 0
    },
    reset_level: {
        type: Number,
        label: 'Reset Level',
        min: 0
    },
    multiplier: {
        type: Number,
        label: 'Multiplier',
        min: 1
    },
    notes: {
        type: String,
        label: 'Notes',
        optional: true
    },
    knitter: {
        type: String,
        label: 'Knitter',
    },
    sort_order: {
        type: Number,
        label: 'Sort order',
        min: 0
    },
    alerts: {
        type: Array,
    },
   'alerts.$': {
        type: String,
    },
    links: {
        type: Array,
    },
    'links.$': {
        type: String,
    },

})
Counts.attachSchema(CountSchema);

/*
if (Meteor.isCordova) {
    Ground.Collection(Links);
}
*/


Meteor.methods({
    'counts.increment'(countId) {
        check(countId,String);
        const count = Counts.findOne(countId);
        const project = Projects.findOne({counts: countId});
        const currentUser = Meteor.userId();
        if (count.knitter == currentUser) {
           Counts.update(countId,{
               $inc: { count: 1 }
           })
        } else {
            throw new Meteor.Error('not-authorized',"Users may only modify their own counts");
        }
        Meteor.call('projects.depart',project._id);
        Meteor.call('counts.check-dependent',count._id,true);
    },
    'counts.decrement'(countId) {
        check(countId,String);
        const count = Counts.findOne(countId);
        const project = Projects.findOne({counts: countId});
        const currentUser = Meteor.userId();
        if (count.knitter == currentUser) {
            if (count.count == 0 ) {
                // leave it where it is
            } else {
                Counts.update(countId,{
                    $inc: { count: -1 }
                })
            }
        } else {
            throw new Meteor.Error('not-authorized',"Users may only modify their own counts");
        }
        Meteor.call('projects.depart',project._id);
        Meteor.call('counts.check-dependent',count._id,false);
    },
    'counts.move-up'(countId) {
        check(countId,String);
        Counts.update(countId, {
            $inc: {sort_order: -1}
        })
    },
    'counts.move-down'(countId) {
        check(countId,String);
        Counts.update(countId,{
            $inc: { sort_order: 1 }
        })
    },
    'counts.edit'(countId,name,notes,count,auto_reset,reset_level,multiplier) {
        check(countId,String);
        check(name,String);
        check(notes,String);
        check(count,String);
        check(auto_reset,String);
        check(reset_level,String);
        check(multiplier,String);
        Counts.update(countId, {$set: {
            name: name,
            notes: notes,
            count: count,
            auto_reset: auto_reset,
            reset_level: reset_level,
            multiplier: multiplier
        }});
    },
    'counts.check-dependent'(countId,up){
        check(countId,String);
        check(up,Boolean);
        const count = Counts.findOne(countId);
        const project = Projects.findOne({counts: countId});

        if (! Meteor.userId()) {
            throw new Meteor.Error('not-authorized',"Log in to follow links.");
        }

        /* Deal with any links that may exist, making sure there are no loops.
         * See this Stack Overflow question for discussion of this solution:
         * http://stackoverflow.com/questions/43585128/meteor-detecting-infinite-loop
         */
        if (project.alreadyVisited.indexOf(countId) != -1 ) {
            if (Meteor.isClient) {
                Materialize.toast("Link chain has looped around!", 3000, 'blue-grey');
            }
        } else {
            Meteor.call('projects.visit',project._id,countId);
            count.links.forEach(function (linkId) {
                var updated = false;
                const link = Links.findOne(linkId);
                const primary = Counts.findOne(link.primary);
                const secondary = Counts.findOne(link.secondary);

                const currentUser = Meteor.userId();
                if (primary.knitter != currentUser || secondary.knitter != currentUser || link.knitter != currentUser) {
                    throw new Meteor.Error('not-authorized',"Users may only modify their own counts");
                }

                if (link.type == 0) {
                    if (primary.count == link.level && up) {
                        updated = true;
                        Counts.update(secondary._id, {$set: {count: 0}});
                        if (Meteor.isClient) {
                            Materialize.toast('"' + primary.name + '" reset "' + secondary.name + '"!', 1000, 'blue-grey');
                        }
                    }
                    // The effect must be undone when going backwards
                    // TODO: These next two sections involve duplication and could perhaps be cleaned up.
                } else if (link.type == 1) {
                    if (up) {
                        if (primary.count % link.level == 0) {
                            updated = true;
                            Counts.update(secondary._id, {$inc: {count: 1}});
                            if (Meteor.isClient) {
                                Materialize.toast('"' + primary.name + '" increased "' + secondary.name + '"!', 1000, 'blue-grey');
                            }
                        }
                    } else {
                        if ((primary.count + 1) % link.level == 0) {
                            updated = true;
                            if (secondary.count > 0) {
                                Counts.update(secondary._id, {$inc: {count: -1}});
                                if (Meteor.isClient) {
                                    Materialize.toast('"' + primary.name + '" decreased "' + secondary.name + '"!', 1000, 'blue-grey');
                                }
                            }
                        }
                    }
                    // The effect must be undone when going backwards - as above
                } else if (link.type == 2) {
                    if (!up) {
                        if ((primary.count + 1) % link.level == 0) {
                            updated = true;
                            if (secondary.count > 0) {
                                Counts.update(secondary._id, {$inc: {count: -1}});
                                if (Meteor.isClient) {
                                    Materialize.toast('"' + primary.name + '" decreased "' + secondary.name + '"!', 1000, 'blue-grey');
                                }
                            }
                        }
                    } else {
                        if (primary.count % link.level == 0) {
                            updated = true;
                            Counts.update(secondary._id, {$inc: {count: 1}});
                            if (Meteor.isClient) {
                                Materialize.toast('"' + primary.name + '" increased "' + secondary.name + '"!', 1000, 'blue-grey');
                            }
                        }
                    }
                }
                if (updated) {
                    Meteor.call('counts.check-dependent',secondary._id,up);
                }
            })
        }

        // Set off any alerts which need setting off...
        count.alerts.forEach(function(alertId) {
            const alert = Alerts.findOne(alertId);
            if (alert.alert == count.count && Meteor.isClient) {
                $("#" + countId + "-alert").empty();
                $("#" + countId + "-counting").toggle();
                Blaze.renderWithData(Template.Alert, {message: alert.message, countId: countId}, $("#" + countId + "-alert")[0]);
                Materialize.toast("Alert triggered: " + alert.message, 3000, 'blue-grey');
            }
        });

        // Auto-reset to the appropriate reset level
        if (count.auto_reset > 0) {
            if (count.count == count.auto_reset) {
                Counts.update(countId, {$set: {count: count.reset_level}});
                if (Meteor.isClient) {
                    Materialize.toast('"' + count.name + '" auto-reset to ' + count.reset_level + "!", 1000, 'blue-grey');
                }
            }
        }

    },
    'counts.remove'(countId) {
        check(countId,String);
        Counts.remove(countId);
    },
    'counts.add-alert'(countId,alert,message) {
       check(countId,String);
       check(message,String);
       check(alert,String);
       const count = Counts.findOne(countId);
       const project = Projects.findOne({counts: countId});
       const alertValue = parseInt(alert);


       if (! count) {
           throw new Meteor.Error('not-found',"That count can't be found!");
       }

       if (! project) {
           throw new Meteor.Error('not-found',"That project can't be found!");
       }

        // Make sure the user is logged in before inserting a task
        if (project.knitter != Meteor.userId()) {
            throw new Meteor.Error('not-authorized',"You may only add alerts to your own counts!");
        }

        // Create a project
        alert = {
            alert: alertValue,
            message: message,
            knitter: Meteor.userId(),
        };
        AlertSchema.validate(alert);
        newalert = Alerts.insert(alert);
        Projects.update(project._id,{ $push: { alerts: newalert }})
        Counts.update(countId,{ $push: { alerts: newalert }})

    },

});

/*
if (Meteor.isCordova) {
    if ( Meteor.isClient ) {
        Ground.methodResume([
            'increment',
            'decrement',
            'move-up',
            'move-down',
            'edit',
            'check-dependent',
            'remove',
            'add-alert'
        ]);
    }
}
*/
