/**
 * Created by milo on 07/04/2017.
 */


import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import SimpleSchema from 'simpl-schema';

//import './counts.js'
import './links.js'

Links = new Meteor.Collection('links');
LinkSchema = new SimpleSchema({
    primary: {
        type: String,
        label: 'Primary' // the agent
    },
    secondary: {
        type: String,
        label: 'Secondary' // the patient
    },
    level: {
        type: Number,
        label: 'Level' // when the agent acts
    },
    type: {
        type: Number,
        label: 'Type' // how the agent acts
    },
    knitter: {
        type: String,
        label: 'Knitter',
    },
});
Links.attachSchema(LinkSchema);

/*
if (Meteor.isCordova) {
    Ground.Collection(Links);
}
*/


Meteor.methods({
    'links.remove'(linkId) {
        check(linkId,String);
        Links.remove(linkId);
    },
    'links.delete-from-all'(linkId,projectId) {
        check(linkId,String);
        check(projectId,String);
        Links.remove(linkId);
        Projects.update(projectId,{$pull: {links: linkId}});
        const project = Projects.findOne(projectId);
        Counts.find({_id: {$in: project.counts}}).forEach(function(count) {
            Counts.update({_id: count._id},{$pull: {links: linkId}});
        });
    }
})

/*
if (Meteor.isCordova) {
    if ( Meteor.isClient ) {
        Ground.methodResume([
            'remove',
            'delete-from-all',
        ]);
    }
}
*/


 /*
 * Type is:
 * 0: reset
 * 1: increase
 * 2: decrease
 */
