/**
 * Created by milo on 24/01/2017.
 */
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import SimpleSchema from 'simpl-schema';

import './counts.js'
import './alerts.js'



Projects = new Meteor.Collection('projects');
ProjectSchema = new SimpleSchema({
    name: {
        type: String,
        label: 'Name'
    },
    notes: {
        type: String,
        label: 'Notes',
        optional: true
    },
    knitter: {
        type: String,
        label: 'Knitter',

    },
    createdAt: {
        type: Date,
        label: 'Created At',
    },
    counts: {
        type: Array,
    },
    'counts.$': {
        type: String,
    },
    alerts: {
        type: Array,
    },
    'alerts.$': {
        type: String,
    },
    links: {
        type: Array,
    },
    'links.$': {
        type: String,
    },
    alreadyVisited: {
        type: Array,
    },
    'alreadyVisited.$': {
        type: String,
    },

    sort_order: {
        type: Number,
        label: 'Sort order',
        min: 0
    }
});
Projects.attachSchema(ProjectSchema);

/*
if (Meteor.isCordova) {
    Ground.Collection(Links);
}
*/

Meteor.methods({
    'projects.insert'(name,notes,counts,count_notes) { // add counts later
        check(name,String);
        check(notes,String);
        check(counts,[String]);
        check(count_notes,[String]);

        // Make sure the user is logged in before inserting a task
        if (! Meteor.userId()) {
            throw new Meteor.Error('not-authorized',"Log in to create a project");
        }

        // Create a project
        project = {
            name: name,
            notes: notes,
            knitter: Meteor.userId(),
            counts: [],
            alerts: [],
            createdAt: new Date(),
            sort_order: 0,
            links: [],
            alreadyVisited: []
        };
        ProjectSchema.validate(project);

        // Add counts to the project
        //counts.forEach(function(count) {
        for (i = 0; i < counts.length; i++) {
            if (count_notes[i] === 'none') {
                var notes = null
            } else {
                var notes = count_notes[i];
            }
            count = {
                name: counts[i],
                notes: notes,
                knitter: Meteor.userId(),
                count: 0,
                auto_reset: 0,
                reset_level: 0,
                multiplier: 1,
                sort_order: i,
                alerts: [],
                links: []
            };
            CountSchema.validate(count);
            newcount = Counts.insert(count);
            project.counts.push(newcount);
        };

        // TODO: Clean up if the creation fails
        Projects.insert(project);
        return project._id

    },
    'projects.remove'(projectId) {
        check(projectId,String);
        project = Projects.findOne(projectId);

        if (! project) {
            throw new Meteor.Error('not-available',"That project count not be found: " + projectId);
        }

        if (! Meteor.userId()) {
            throw new Meteor.Error('not-authorized',"Log in to delete projects.");
        }

        if (Meteor.userId() != project.knitter) {
            throw new Meteor.Error('not-authorized',"Only owners can delete projects: " + Meteor.userId() + "/" + project.knitter);
        }

        // Before deleting a project its associated data must also be cleaned up
        project.counts.forEach(function(count){
            Meteor.call('counts.remove',count);
        })
        project.alerts.forEach(function(alert){
            Meteor.call('alerts.remove',alert);
        })
        Projects.remove(projectId);

    },
    'projects.move-up'(projectId) {
        check(projectId,String);
        Projects.update(projectId,{
            $inc: { sort_order: -1 }
        })
    },
    'projects.move-down'(projectId) {
        check(projectId,String);
        Projects.update(projectId,{
            $inc: { sort_order: 1 }
        })
    },
    'projects.reset-counts'(projectId) {
        check(projectId,String);
        const project = Projects.findOne({_id: projectId});

        if (! Meteor.userId()) {
            throw new Meteor.Error('not-authorized',"Log in to delete projects.");
        }

        if (Meteor.userId() != project.knitter) {
            throw new Meteor.Error('not-authorized',"Only owners can reset counts: " + Meteor.userId() + "/" + project.knitter);
        }

        const counts = Counts.find({_id: { $in: project.counts }},{sort: {name: 1}}).fetch();
        for (i = 0; i < counts.length; i++) {
           Counts.update(counts[i]._id,{$set: {sort_order: i}})
        }
    },
    // This should be called once the 'stop moving' button is tapped
    'projects.align-counts'(countIds) {
        check(countIds,[String]);
        for (i = 0; i < countIds.length; i++) {
            Counts.update(countIds[i],{$set: {sort_order: i}});
        }
    },
    'projects.reset-projects'() {
        const projects = Projects.find({},{$sort: {created_at: -1}}).fetch();
        for (i = 0; i < projects.length; i++) {
           Projects.update(projects[i]._id,{$set: {sort_order: i}})
        }
    },
    'projects.edit'(projectId,name,notes) {
        check(projectId,String);
        check(name,String);
        check(notes,String);
        Projects.update(projectId, {$set: {name: name, notes: notes}});
    },
    'projects.add-count'(projectId,name,notes) {
        check(projectId,String);
        check(name,String);
        check(notes,String);
        const project = Projects.findOne({_id: projectId});
        order = 0;
        if (project.counts.length > 0) {
            for (i = 0; i < project.counts.length; i++) {
                var thisCount = Counts.findOne(project.counts[i]);
                if (thisCount.sort_order > order) {
                    order = thisCount.sort_order;
                }
            }
            order = order + 1;
        }
        count = {
            name: name,
            notes: notes,
            knitter: Meteor.userId(),
            count: 0,
            auto_reset: 0,
            reset_level: 0,
            multiplier: 1,
            sort_order: order,
            alerts: [],
            links: []
        };
        CountSchema.validate(count);
        newcount = Counts.insert(count);
        Projects.update(projectId,{ $push: { counts: newcount }})
    },
    'projects.delete-count'(projectId,countId) {
        check(projectId,String);
        check(countId,String);
        const project = Projects.findOne(projectId);
        const count = Counts.findOne(countId);
        if (! project || ! count) {
            throw new Meteor.Error('not-available',"That project/count not be found: " + projectId);
        }
        if (! Meteor.userId()) {
            throw new Meteor.Error('not-authorized',"Log in to delete projects.");
        }

        if (Meteor.userId() != project.knitter) {
            throw new Meteor.Error('not-authorized',"Only owners can delete projects: " + Meteor.userId() + "/" + project.knitter);
        }
        Projects.update(projectId,{$pull: {counts: countId}});
        Counts.remove({_id: countId});
    },
    'projects.add-link'(projectId,primary,secondary,type,level) {
        check(projectId,String);
        check(primary,String);
        check(secondary,String);
        check(type,Number);
        check(level,String);
        const project = Projects.findOne({_id: projectId});
        if (primary == secondary) {
            throw new Meteor.Error('not-allowed', "A count cannot be linked to itself.");
        }
        const p = Counts.findOne({_id: primary});
        const s = Counts.findOne({_id: secondary});
        if (!p || !s) {
            throw new Meteor.Error('not-allowed', "One or both of the linked counts don't exist.");
        }
        if ([0,1,2].indexOf(type) == -1) {
            throw new Meteor.Error('not-allowed', "The correct link type must be specified.");
        }

        link = {
            primary: primary,
            secondary: secondary,
            level: parseInt(level),
            type: type,
            knitter: Meteor.userId(),
        }
        LinkSchema.validate(link);
        const newlink = Links.insert(link);
        Projects.update(projectId,{$push: {links: newlink}});
        Counts.update(primary,{$push: {links: newlink}});
    },
    'projects.visit'(projectId,countId) {
        check(projectId,String);
        check(countId,String);
        const project = Projects.findOne({_id: projectId});
        const count = Counts.findOne({_id: countId});
        if (! Meteor.userId()) {
            throw new Meteor.Error('not-authorized',"Log in to delete projects.");
        }
        if (Meteor.userId() != project.knitter) {
            throw new Meteor.Error('not-authorized',"Only owners can delete projects: " + Meteor.userId() + "/" + project.knitter);
        }
        Projects.update(projectId,{$push: {alreadyVisited: countId}})
    },
    'projects.depart'(projectId) {
        check(projectId,String);
        const project = Projects.findOne({_id: projectId});
        if (! Meteor.userId()) {
            throw new Meteor.Error('not-authorized',"Log in to delete projects.");
        }
        if (Meteor.userId() != project.knitter) {
            throw new Meteor.Error('not-authorized',"Only owners can delete projects: " + Meteor.userId() + "/" + project.knitter);
        }
        Projects.update(projectId,{$set: {alreadyVisited: []}})
    }

});

/*
if (Meteor.isCordova) {
    if ( Meteor.isClient ) {
        Ground.methodResume([
            'insert',
            'remove',
            'move-up',
            'move-down',
            'reset-counts',
            'reset-projects',
            'edit',
            'add-count',
            'delete-count',
            'add-link'
        ]);
    }
}
*/
