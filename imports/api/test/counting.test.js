/**
 * Created by milo on 13/02/2017.
 */

import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { assert } from 'meteor/practicalmeteor:chai';
import { sinon } from 'meteor/practicalmeteor:sinon';
import { resetDatabase } from 'meteor/xolvio:cleaner';
import  StubCollections  from 'meteor/hwillson:stub-collections';


import '../projects';
import '../counts.js';

if (Meteor.isServer) {
    describe('Counts', () => {
        describe('methods', () => {
            const userId = Random.id();
            let countId;
            let countId2;
            let projectId;

            beforeEach(() => {
                resetDatabase();
                StubCollections.stub(Counts);
                StubCollections.stub(Projects);
                sandbox = sinon.sandbox.create();
                countId = Counts.insert({
                    name: 'test count',
                    knitter: userId,
                    count: 0,
                    auto_reset: 0,
                    reset_level: 1,
                    multiplier: 1,
                    sort_order: 0,
                    alerts: []
                });
                countId2 = Counts.insert({
                    name: 'test count2',
                    knitter: userId,
                    count: 0,
                    auto_reset: 0,
                    reset_level: 1,
                    multiplier: 1,
                    sort_order: 1,
                    alerts: []
                });
                projectId = Projects.insert({
                    name: 'awesome test project',
                    notes: 'notes for test project',
                    knitter: userId,
                    counts: [countId,countId2],
                    createdAt: new Date(),
                    alerts: [],
                    sort_order: 0,
                    alreadyVisited: []
                });
            });

            afterEach(function () {
                sandbox.restore();
                StubCollections.restore();
            })

            it('moves counts up and down', () => {
                sandbox.stub(Meteor,'userId').returns(userId);
                const project = Projects.findOne(projectId);
                console.log("PROJECT: " + project.name);
                for (i = 0; i < project.counts.length; i++) {
                    var count = Counts.findOne(project.counts[i]);
                    assert.equal(i,count.sort_order);
                }
                const moveUp = Meteor.server.method_handlers['counts.move-up'];
                const moveDown = Meteor.server.method_handlers['counts.move-down'];
                const resetPosition = Meteor.server.method_handlers['projects.reset-counts'];
                const invocation = { userId };
                moveDown.apply(invocation,[countId]);
                assert.equal(Counts.findOne(countId).sort_order,1);
                moveDown.apply(invocation,[countId]);
                assert.equal(Counts.findOne(countId).sort_order,2);
                moveUp.apply(invocation,[countId]);
                assert.equal(Counts.findOne(countId).sort_order,1);
                resetPosition.apply(invocation,[projectId])
                for (i = 0; i < project.counts.length; i++) {
                    var count = Counts.findOne(project.counts[i]);
                    assert.equal(i,count.sort_order);
                }
            })



        });
    })
}

if (Meteor.isClient) {
    describe('Counting Activity', () => {
        describe('UI', () => {
            const userId = Random.id();
            let countId;
            let projectId;

            beforeEach(() => {
                resetDatabase();
                StubCollections.stub(Counts);
                StubCollections.stub(Projects);
                sandbox = sinon.sandbox.create();
                countId = Counts.insert({
                    name: 'test count',
                    knitter: userId,
                    count: 0,
                    auto_reset: 0,
                    reset_level: 1,
                    multiplier: 1,
                    alerts: [],
                    sort_order: 0
                });
                projectId = Projects.insert({
                    name: 'test project',
                    notes: 'notes for test project',
                    knitter: userId,
                    counts: [countId],
                    createdAt: new Date(),
                    alerts: [],
                    sort_order: 0,
                    alreadyVisited: []
                });
            });

            afterEach(function () {
                sandbox.restore();
                StubCollections.restore();
            })

            it('can fail to test anything', () => {
                assert.equal(true,true);
            });
        })
    })
}
