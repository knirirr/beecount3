/**
 * Created by milo on 08/02/2017.
 */
/* meteor test --driver-package practicalmeteor:mocha */
/* eslint-env mocha */

import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Random } from 'meteor/random';
import { assert } from 'meteor/practicalmeteor:chai';
import { sinon } from 'meteor/practicalmeteor:sinon';
import { chai } from 'meteor/practicalmeteor:chai';
import { resetDatabase } from 'meteor/xolvio:cleaner';
import  StubCollections  from 'meteor/hwillson:stub-collections';

import '../projects.js';
import '../counts.js';
import './test-helpers.js';
//import '../../views/list_projects.js';
import { withRenderedTemplate } from './test-helpers.js';


if (Meteor.isServer) {
    describe('Projects', () => {
        describe('methods', () => {
            const userId = Random.id();
            let projectId;
            let countId;
            let projectId2;
            let countId2;

            beforeEach(() => {
                resetDatabase();
                StubCollections.stub(Counts);
                StubCollections.stub(Projects);
                sandbox = sinon.sandbox.create();
                countId = Counts.insert({
                    name: 'test count',
                    knitter: userId,
                    count: 0,
                    auto_reset: 0,
                    reset_level: 1,
                    multiplier: 1,
                    sort_order: 0,
                    alerts: [],
                    links: []
                });
                countId2 = Counts.insert({
                    name: 'test count2',
                    knitter: userId,
                    count: 0,
                    auto_reset: 0,
                    reset_level: 1,
                    multiplier: 1,
                    sort_order: 1,
                    alerts: [],
                    links: []
                });
                projectId = Projects.insert({
                    name: 'test project',
                    notes: 'notes for test project',
                    knitter: userId,
                    counts: [countId],
                    createdAt: new Date(),
                    alerts: [],
                    sort_order: 0,
                    alreadyVisited: [],
                    links: []
                });
                projectId2 = Projects.insert({
                    name: 'test project2',
                    notes: 'notes for test project',
                    knitter: userId,
                    counts: [countId2],
                    createdAt: new Date(),
                    alerts: [],
                    sort_order: 0,
                    alreadyVisited: [],
                    links: []
                });
            });

            afterEach(function () {
                sandbox.restore();
                StubCollections.restore();
            })

            it('can delete owned project', () => {
                sandbox.stub(Meteor,'userId').returns(userId);
                assert.equal(Projects.find().count(), 2);
                assert.equal(Counts.find().count(), 2);
                const deleteProject = Meteor.server.method_handlers['projects.remove'];
                const invocation = { userId };
                deleteProject.apply(invocation,[projectId]);
                assert.equal(Projects.find().count(), 1);
                assert.equal(Counts.find().count(), 1);
            });

            it('cannot delete unowned project', () => {
                sandbox.stub(Meteor,'userId').returns(Random.id());
                assert.equal(Projects.find().count(), 2);
                assert.equal(Counts.find().count(), 2);
                const deleteProject = Meteor.server.method_handlers['projects.remove'];
                const invocation = { userId };
                assert.throws(() => {
                    deleteProject.apply(invocation, [projectId]);
                }, Meteor.Error, '[not-authorized]');
                assert.equal(Projects.find().count(), 2);
                assert.equal(Counts.find().count(), 2);
            });

            it('must log in to add project', () => {
                sandbox.stub(Meteor,'userId').returns(null);
                const addProject = Meteor.server.method_handlers['projects.insert'];
                const invocation = { userId };
                const counts = ['one','two']
                const notes = ['wibble','ftang']
                assert.throws(() => {
                    addProject.apply(invocation, ['test','notes',counts,notes]);
                }, Meteor.Error, '[not-authorized]');
                assert.equal(Projects.find().count(), 2);
                assert.equal(Counts.find().count(), 2);
            });

            it('can add project when logged in', () => {
                sandbox.stub(Meteor,'userId').returns(userId);
                assert.equal(Projects.find().count(), 2);
                assert.equal(Counts.find().count(), 2);
                const addProject = Meteor.server.method_handlers['projects.insert'];
                const invocation = { userId };
                const counts = ['one','two']
                const count_notes = ['note1','note2']
                addProject.apply(invocation, ['test','notes',counts,count_notes]);
                assert.equal(Projects.find().count(), 3);
                assert.equal(Counts.find().count(), 4);
            });

            it('can increment and decrement count', () => {
                sandbox.stub(Meteor,'userId').returns(userId);
                var currCount = Counts.findOne(countId).count;
                assert.equal(currCount,0);
                const countUp = Meteor.server.method_handlers['counts.increment'];
                const countDown = Meteor.server.method_handlers['counts.decrement'];
                const invocation = { userId };
                countUp.apply(invocation,[countId]);
                currCount = Counts.findOne(countId).count;
                assert.equal(currCount,1);
                countDown.apply(invocation,[countId]);
                currCount = Counts.findOne(countId).count;
                assert.equal(currCount,0);
            })

            it('refuses to let the wrong user count', () => {
                sandbox.stub(Meteor,'userId').returns(null);
                var currCount = Counts.findOne(countId).count;
                assert.equal(currCount,0);
                const countUp = Meteor.server.method_handlers['counts.increment'];
                const countDown = Meteor.server.method_handlers['counts.decrement'];
                const invocation = { userId };
                assert.throws(() => {
                    countUp.apply(invocation, [countId]);
                }, Meteor.Error, '[not-authorized]');
                var currCount = Counts.findOne(countId).count;
                assert.equal(currCount,0);
                assert.throws(() => {
                    countDown.apply(invocation, [countId]);
                }, Meteor.Error, '[not-authorized]');
                var currCount = Counts.findOne(countId).count;
                assert.equal(currCount,0);

            })

            it('moves projects up and down', () => {
                sandbox.stub(Meteor,'userId').returns(userId);
                const projects = Projects.find();
                for (i = 0; i < projects.length; i++) {
                    assert.equal(i,projects[i].sort_order)
                }
                const moveUp = Meteor.server.method_handlers['projects.move-up'];
                const moveDown = Meteor.server.method_handlers['projects.move-down'];
                const resetPosition = Meteor.server.method_handlers['projects.reset-projects'];
                const invocation = { userId };
                moveDown.apply(invocation,[projectId]);
                assert.equal(Projects.findOne(projectId).sort_order,1);
                moveDown.apply(invocation,[projectId]);
                assert.equal(Projects.findOne(projectId).sort_order,2);
                moveUp.apply(invocation,[projectId]);
                assert.equal(Projects.findOne(projectId).sort_order,1);
                resetPosition.apply(invocation,[])
                for (i = 0; i < projects.length; i++) {
                    assert.equal(i,projects[i].sort_order)
                }
            })



        });
    })
}

if (Meteor.isClient) {
    describe('Projects', () => {
        describe('UI', () => {
            const userId = Random.id();
            let projectId;
            let countId;

            beforeEach(() => {
                resetDatabase();
                Template.registerHelper('_', key => key);
                StubCollections.stub(Counts);
                StubCollections.stub(Projects);
                sandbox = sinon.sandbox.create();
                countId = Counts.insert({
                    name: 'test count',
                    knitter: userId,
                    count: 0,
                    auto_reset: 0,
                    reset_level: 1,
                    multiplier: 1,
                    alerts: [],
                    sort_order: 0
                });
                projectId = Projects.insert({
                    name: 'test project',
                    notes: 'notes for test project',
                    knitter: userId,
                    counts: [countId],
                    createdAt: new Date(),
                    alerts: [],
                    sort_order: 0,
                    alreadyVisited: []
                });
            });

            afterEach(function () {
                sandbox.restore();
                Template.deregisterHelper('_');
                StubCollections.restore();
            })

            /*
            it('renders project list correctly', function () {
                const projects = Projects.find({});
                withRenderedTemplate('ListProjects', projects, el => {
                    chai.assert.equal($(el).find('input[type=text]').val(), todo.text);
                    chai.assert.equal($(el).find('.list-item.checked').length, 0);
                    chai.assert.equal($(el).find('.list-item.editing').length, 0);
                });
            });
            */



        });
    })


}