/**
 * Created by milo on 14/02/2017.
 */

import { Session } from 'meteor/session';

Template.MainLayout.onRendered(function () {
    $(".dropdown-button").dropdown({hover: false});
    if (Reload.isWaitingForResume()) {
        Blaze.render(Template.Reload, $("#errors-go-here")[0]);
    }
});

Template.MainLayout.helpers({
    status: function() {
        return Session.get('status');
    },
    statustext: function() {
        return Session.get('statustext');
    }

})

Template.Reload.events({
    'click #reload'() {
        window.location.replace(window.location.href);
    }
})


Template.MoveElements.events({
    'click .start-moving'() {
        $('.stop-moving').toggle();
        $('.start-moving').toggle();
        $('.move-up').toggle(600);
        $('.move-down').toggle(600);
        $('.sort-position').toggle(600);
        $('.reset-position').toggle();
    },
    'click .stop-moving'() {
        $('.stop-moving').toggle();
        $('.start-moving').toggle();
        $('.move-up').toggle(600);
        $('.move-down').toggle(600);
        $('.sort-position').toggle(600);
        $('.reset-position').toggle();
        var type = $('#object-type').html();
        /*
         * Upon stopping it is essential to reset all the values so that there are none with the same sort_order.
         * If there are then the counts will shift position upon counting.
         */
        if (type == 'count') {
            var ids = [];
            const elements = $('.count-boxes > .count-box');
            for (i = 0; i < elements.length; i++) {
                var element = elements[i];
                console.log(element);
                const id = element.id.replace("-counting","");
                ids.push(id);
            }
            Meteor.call('projects.align-counts',ids);
        }
    },
    /*
     * As a dreadful hack the necessary information has been hidden in the html and some
     * jquery used to fish it out again.
     */
    'click .reset-position'() {
        var type = $('#object-type').html();
        if (type == 'count') {
            var projectId = $('#object-id').html();
            Meteor.call('projects.reset-counts',projectId);
        } else if (type == 'project') {
            Meteor.call('projects.reset-projects');
        }
    }
})
