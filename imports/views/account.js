
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './account.html';

Template.Account.onCreated(function() {
    var self = this;
    self.autorun(function() {
        Meteor.subscribe('allusers');
        ClientStorage.set('beecount_location', '/account');
    })
});


Template.Account.helpers({
    projects() {
        return Projects.find({}, { sort: { sort_order: 1 } });
    },
    users() {

        return Meteor.users.find({});
    },
    empty() {
        if (Meteor.users.find({}).count() === 0) {
            return true;
        }
        return false;
    },
    isReady: function(sub) {
        if(sub) {
            return FlowRouter.subsReady(sub);
        } else {
            return FlowRouter.subsReady();
        }
    },


});

Template.Account.events({
    'click .delete'() {
        var self = this;
        $('.' + self._id + ' > .delete').toggle();
        $('.' + self._id + ' > .really-delete').toggle();
        $('.' + self._id + ' > .cancel-delete').toggle();
        // TODO: Redirect to home page, log out.
    },
    'click .really-delete'() {
        var self = this;
        Meteor.call('auth.remove_user', self._id, function (error) {
            if (error) {
                $('.' + self._id + ' > .delete').toggle();
                $('.' + self._id + ' > .really-delete').toggle();
                $('.' + self._id + ' > .cancel-delete').toggle();
                Materialize.toast('Failed to delete account!', 3000, 'orange darken-4');
            } else {
                Materialize.toast('Account deleted!', 3000, 'blue-grey');
            }
        });

    },
    'click .cancel-delete'() {
        var self = this;
        $('.' + self._id + ' > .delete').toggle();
        $('.' + self._id + ' > .really-delete').toggle();
        $('.' + self._id + ' > .cancel-delete').toggle();
    },
})

Template.User.helpers({
    get_address: function() {
        //return JSON.parse(this.User).emails[0].address;
        //return JSON.parse(this.User)._id;
        return this.emails[0].address;
    }
});
