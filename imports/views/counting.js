/**
 * Created by milo on 09/02/2017.
 */

import { ReactiveDict } from 'meteor/reactive-dict';

import '../api/projects.js'
import '../api/counts.js'
import '../api/alerts.js'
import './counting.html'

/*
TODO: Fix exception: http://stackoverflow.com/questions/30936603/meteor-exception-in-template-helper#30936749
TODO: Exception in template helper: project@http://localhost:3000/app/app.js?hash=a127f3e15ae06b60b8265988bad2516b5245c97a:991:54
 */


Template.Counting.onCreated(function() {
    var self = this;
    self.autorun(function() {
        var projectId = FlowRouter.getParam('projectId');
        self.subscribe('single-project',projectId);
        self.subscribe('project-counts',projectId);
        self.subscribe('project-alerts',projectId);
        self.subscribe('project-links',projectId);
        ClientStorage.set('beecount_location', '/count/' + projectId);
    });
})


Template.Counting.helpers({
    currentCounts: function() {
        var projectId = FlowRouter.getParam('projectId');
        const project = Projects.findOne(projectId);
        var counts = Counts.find({
            _id: { $in: project.counts }
        },
        {
            sort: { sort_order: 1 }
        }
        );
        return counts;
    },
    currentProject: function() {
        var projectId = FlowRouter.getParam('projectId');
        const project = Projects.findOne(projectId);
        return project;
    },
    ifNotZero: function(array) {
        if (array.length > 0) {
            return true;
        }
        return false;
    }
})

Template.Summary.helpers({
    summary: function() {
       var comments = [];
       var projectId = FlowRouter.getParam('projectId');
       const project = Projects.findOne(projectId);
       Counts.find({_id: { $in: project.counts }}).map(function(count) {
          if (count.auto_reset > 0) {
              comments.push('Count "' + count.name + '" will auto-reset to ' + count.reset_level + ' at '
                  + count.auto_reset + ".");
          }
          count.alerts.forEach(function(alertId){
              const alert = Alerts.findOne(alertId);
              if (alert) {
                  comments.push('Count "' + count.name + '" will show an alert at ' + alert.alert + '.');
              }
          })
       });
       Links.find({_id: {$in: project.links}}).map(function(link) {
       //Links.find({}).map(function(link) {
           const primary = Counts.findOne(link.primary);
           const secondary = Counts.findOne(link.secondary);
           var type = '';
           var when = 'every';
           switch(link.type) {
               case 0:
                   type = ' reset ';
                   when = 'at';
                   break;
               case 1:
                   type = ' increase ';
                   break;
               case 2:
                   type = ' decrease ';
                   break;
               default:
                   type = " do something or other to ";
           }
           comments.push('Count "' + primary.name + '" will ' + type + ' "' + secondary.name + '"' + when + ' ' + link.level + ".");
       });
       return comments;
    }
})

Template.CountBox.helpers({
    countMultiplied: function(id) {
        const count = Counts.findOne(id);
        var result = parseInt(count.count) * parseInt(count.multiplier);
        return result;
    }
})


Template.CountBox.events({
    'click .increment'(event) {
        const countId = this._id;
        Meteor.call('counts.increment',countId);
        //checkAlerts(countId);
    },
    'click .decrement'(event) {
        const countId = this._id;
        Meteor.call('counts.decrement',countId);
        //checkAlerts(countId);
    },
    'click .move-up'() {
        var self = this;
        Meteor.call('counts.move-up',self._id);
    },
    'click .move-down'() {
        var self = this;
        Meteor.call('counts.move-down',self._id);
    },
})


Template.Alert.events({
    'click .dismiss-alert'(event) {
        const data = Blaze.getData(event.target);
        const countId = data['countId'];
        $("#" + countId + "-counting").toggle();
        $("#" + countId + "-alert").empty();
    }
})

