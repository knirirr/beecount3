/**
 * Created by milo on 14/03/2017.
 */

import { Meteor } from 'meteor/meteor';

import '../api/projects.js'
import '../api/counts.js'
import './edit_count.html'

Template.EditCount.onCreated(function() {
    var self = this;
    self.autorun(function() {
        var countId = FlowRouter.getParam('countId');
        self.subscribe('single-count',countId);
        self.subscribe('project-from-count',countId);
        self.subscribe('count-alerts',countId);
        ClientStorage.set('beecount_location', '/edit_count/' + countId);
    })
})

Template.EditCount.helpers({
    currentCount: function() {
        var countId = FlowRouter.getParam('countId');
        const count = Counts.findOne({_id: countId}) || {};
        return count;
    },
    currentProject: function() {
        var countId = FlowRouter.getParam('countId');
        const project = Projects.findOne({counts: countId}) || {};
        return project;
    },
    currentAlerts: function() {
        var countId = FlowRouter.getParam('countId');
        return Alerts.find();
    }
    /*
    isReady: function(sub) {
        if(sub) {
            return FlowRouter.subsReady(sub);
        } else {
            return FlowRouter.subsReady();
        }
    }
    */
})

Template.EditCount.events({
    'click #edit-count'(event) {
        var countId = FlowRouter.getParam('countId');
        const project = Projects.findOne({counts: countId});
        const projectId = project._id;

        // Get values manually from form element
        const name = $("#name").val();
        const notes = $("#notes").val();
        const count = $("#count").val();
        const auto_reset = $("#auto_reset").val();
        const reset_level = $("#reset_level").val();
        const multiplier = $("#multiplier").val();
        const alert = $("#new-alert").val();
        const alert_msg = $("#new-alert-message").val();



        // Perform validation
        var errors = [];
        if (!name) {
            errors.push("You'll need to give this count a name.");
        }
        if (!count) {
            errors.push("You'll need to give this count a value.");
        }
        if (count != parseInt(count,10)) {
            errors.push("The count value needs to be a number.");
        }
        if (auto_reset) {
            if (auto_reset != parseInt(auto_reset,10)) {
                errors.push("The auto_reset value needs to be a number.");
            }
        } else {
            errors.push("The Auto Reset value must be specified.");
        }
        if (reset_level) {
            if (reset_level != parseInt(reset_level,10)) {
                errors.push("The reset_level value needs to be a number.");
            }
        } else {
            errors.push("The Reset Level value must be specified.");
        }
        if (multiplier) {
            if (multiplier != parseInt(multiplier,10)) {
                errors.push("The multiplier value needs to be a number.");
            }
        } else {
            errors.push("The Multiplier must be specified.");
        }
        if (alert || alert_msg) {
            errors.push("Did you mean to add an alert? Tap the 'Add Alert' button before saving if so.")
        }

        // Clear existing error area and render new errors, if
        if (errors.length > 0) {
            $("#errors-go-here").empty();
            Blaze.renderWithData(Template.ValidationErrors, {errors: errors}, $("#errors-go-here")[0])
            // TODO: Find out where this came from.
            //$('.modal').modal();
            //$('#validation-errors').modal('open');
            return;
        }

        // TODO: Update the projects and the counts. The project boxes will need to have the ID in them
        Meteor.call('counts.edit', countId, name, notes, count, auto_reset, reset_level, multiplier, function (error) {
            if (error) {
                console.log("Count edit error: " + error);
                Materialize.toast('Count \'' + name + '\' editing failed!', 3000, 'orange darken-4');
                return false;
            } else {
                Materialize.toast('Count \'' + name + '\' edited successfully!', 3000, 'blue-grey');
                FlowRouter.go('/count/' + projectId);
            }

        });

    },
    'click #add-alert'() {
        //TODO: Check for duplicate alert level

        var countId = FlowRouter.getParam('countId');
        var alertLevel = $('#new-alert').val();
        var alertMsg = $('#new-alert-message').val();
        var errors = [];

        const count = Counts.findOne(countId);
        count.alerts.forEach(function(alertId) {
           const alert = Alerts.findOne(alertId);
           if (alert.alert == alertLevel) {
               errors.push("There's already an alert at " + alertLevel + "  for this count.");
           }
        })

        if (!alertLevel) {
            errors.push('You must specify when the alert will be triggered.');
        }
        if (!alertMsg) {
            errors.push('What message will the alert show when triggered?');
        }

        $("#errors-go-here").empty();
        if (errors.length > 0) {
            Blaze.renderWithData(Template.EditProjectErrors, {
                errors: errors
            }, $("#errors-go-here")[0]);
            $(document).scrollTop( $("#errors-go-here").offset().top );
            return;
        }

        Meteor.call('counts.add-alert', countId, alertLevel, alertMsg, function(error) {
            if (error) {
                console.log("Alert add error: " + error);
                Materialize.toast('Failed to add alert \'' + alertLevel + ':' + alertMsg + '\'!', 3000, 'orange darken-4');
                return false;
            } else {
                Materialize.toast('Alert \'' + alertLevel + ':' + alertMsg + '\' added!', 3000, 'blue-grey');
                $('#new-alert').val(null);
                $('#new-alert-message').val(null);
            }
        });
    }
});

Template.AlertDelete.events({
    'click .delete-alert'(event) {
        var self = this;
        $('.' + self._id + ' > .delete-alert').toggle();
        $('.' + self._id + ' > .really-delete').toggle();
        $('.' + self._id + ' > .cancel-delete').toggle();

    },
    'click .really-delete'() {
        var self = this;
        const countId = FlowRouter.getParam('countId');
        const alertId = self._id;
        Meteor.call('alerts.delete-from-all',alertId,countId, function(error){
            if (error) {
                $('.' + countId + ' > .delete-alert').toggle();
                $('.' + countId + ' > .really-delete').toggle();
                $('.' + countId + ' > .cancel-delete').toggle();
                Materialize.toast('Failed to delete alert \'' + self.alert + ':' + self.message + '\'!', 3000, 'orange darken-4');
            } else {
                Materialize.toast('Alert \'' + self.alert + ':' + self.message + '\' deleted!', 3000, 'blue-grey');
            }
        });

    },
    'click .cancel-delete'() {
        var self = this;
        $('.' + self._id + ' > .delete-alert').toggle();
        $('.' + self._id + ' > .really-delete').toggle();
        $('.' + self._id + ' > .cancel-delete').toggle();
    },

})