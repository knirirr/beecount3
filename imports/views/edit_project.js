/**
 * Created by milo on 14/03/2017.
 */

import { Meteor } from 'meteor/meteor';

import '../api/projects.js'
import '../api/counts.js'
import '../api/links.js'
import './edit_project.html'


Template.EditProject.onCreated(function() {
    var self = this;
    self.autorun(function() {
        var projectId = FlowRouter.getParam('projectId');
        self.subscribe('single-project',projectId);
        self.subscribe('project-counts',projectId);
        self.subscribe('project-links',projectId);
        ClientStorage.set('beecount_location', '/edit_project/' + projectId);
    });
})


Template.EditProject.helpers({
    currentProject: function() {
        var projectId = FlowRouter.getParam('projectId');
        const project = Projects.findOne(projectId);
        return project;
    },
    currentCounts: function() {
        var projectId = FlowRouter.getParam('projectId');
        const project = Projects.findOne(projectId);
        var counts = Counts.find({}, { sort: { sort_order: -1 }});
        return counts;
    },
    currentLinks: function() {
        var projectId = FlowRouter.getParam('projectId');
        const project = Projects.findOne(projectId);
        var links = Links.find({ _id: { $in: project.links } }, {});
        return links;
    }
})

Template.EditProject.events({
    'click #edit-project'(event) {
        var projectId = FlowRouter.getParam('projectId');

        // Get values manually from form element
        const name = $("#name").val();
        const notes = $("#notes").val();

        // Perform validation
        var errors = [];
        if (!name) {
            errors.push("You'll need to give this project a name.");
        }

        const project = Projects.findOne(projectId);
        if (project.counts.length == 0) {
            errors.push("Each project must have at least one count.");
        }

        // Clear existing error area and render new errors, if
        $("#errors-go-here").empty();
        if (errors.length > 0) {
            Blaze.renderWithData(Template.ValidationErrors, {errors: errors}, $("#errors-go-here")[0])
            $(document).scrollTop( $("#errors-go-here").offset().top );
            return;
        }

        Meteor.call('projects.edit',projectId,name,notes,function(error) {
            if (error) {
                console.log("Project edit error: " + error);
                Materialize.toast('Project \'' + name + '\' editing failed!', 3000, 'orange darken-4');
                return false;
            } else {
                Materialize.toast('Project \'' + name + '\' edited successfully!', 3000, 'blue-grey');
                FlowRouter.go('/count/' + projectId);
            }

        });
    },
    'click #add-count'(event) {
        //TODO: Check for duplicate count name
        const projectId = FlowRouter.getParam('projectId');
        const countName = $('#new-count').val();
        const countNotes = $('#new-count-notes').val();


        $("#errors-go-here").empty();
        if (!countName) {
            Blaze.renderWithData(Template.EditProjectErrors, {
                errors: ['You must supply a name for the new count.']
            }, $("#errors-go-here")[0]);
            $(document).scrollTop( $("#errors-go-here").offset().top );
            return;
        }

        Meteor.call('projects.add-count',projectId,countName,countNotes, function(error) {
            if (error) {
                console.log("Count add error: " + error);
                Materialize.toast('Failed to add count \'' + countName + '\'!', 3000, 'orange darken-4');
                return false;
            } else {
                Materialize.toast('Count \'' + countName + '\' added!', 3000, 'blue-grey');
                $('#new-count').val(null);
                $('#new-count-notes').val(null);
            }
        });
    },
    'click #add-link'(event) {
        console.log('holy crap');
        const projectId = FlowRouter.getParam('projectId');
        const primary = $("input:radio[name='primary']:checked").val();
        const secondary = $("input:radio[name='secondary']:checked").val();
        const link_type = parseInt($("input:radio[name='link-type']:checked").val());
        const how_often = $('#link-how-often').val();
        var errors = [];

        if (!primary) {
            errors.push("You need to specify from which count you have linked.");
        }
        if (!secondary) {
            errors.push("You need to specify to which count you have linked.");
        }
        if (!(link_type == 0 || link_type == 1 || link_type == 2)) {
            errors.push("Please specify what effect this link should have (increase, decrease or reset).")
        }
        if (!how_often) {
            errors.push("You need to specify when the link will be triggered.")
        }
        if (primary && secondary && primary == secondary) {
            const selfLink = Counts.findOne(primary);
            errors.push('You seem to have linked"' + selfLink.name + '" to itself. Perhaps you need to use an auto-reset or alert instead?');
        }
        if (Links.find({primary: primary, secondary: secondary, type: link_type}).count() > 0) {
            errors.push('You have tried to re-create an existing link. Please check the list of links at the bottom of the screen.');
        }

        $("#errors-go-here").empty();
        if (errors.length > 0) {
            Materialize.toast('Oh noes!', 2000, 'orange darken-4');
            Blaze.renderWithData(Template.ValidationErrors, {errors: errors}, $("#errors-go-here")[0]);
            $(document).scrollTop( $("#errors-go-here").offset().top );
            return;
        }

        Meteor.call('projects.add-link',projectId,primary,secondary,link_type,how_often, function(error) {
            if (error) {
                console.log("Link add error: " + error);
                Materialize.toast('Failed to add link!', 3000, 'orange darken-4');
                return false;
            } else {
                Materialize.toast('Link added!', 3000, 'blue-grey');
            }
        });

    },
    'change .link-selector'(event) {
        const id = event.currentTarget.id;
        if (id.indexOf('p-select') == -1) {
            //s- selected
            $('#' + id.replace('s-','p-')).prop('checked',false);
            //$('#' + id.replace('s-','p-') + '-label').toggle();

        } else {
            $('#' + id.replace('p-','s-')).prop('checked',false);
            //$('#' + id.replace('p-','s-') + '-label').toggle();
        }
    },
});

Template.CountDelete.events({
    'click .delete-count'(event) {
        var self = this;
        $('.' + self._id + ' > .delete-count').toggle();
        $('.' + self._id + ' > .really-delete').toggle();
        $('.' + self._id + ' > .cancel-delete').toggle();

    },
    'click .really-delete'() {
        var self = this;
        var projectId = FlowRouter.getParam('projectId');
        var countId = self._id;
        Meteor.call('projects.delete-count',projectId,countId, function(error){
            if (error) {
                $('.' + countId + ' > .delete-count').toggle();
                $('.' + countId + ' > .really-delete').toggle();
                $('.' + countId + ' > .cancel-delete').toggle();
                Materialize.toast('Failed to delete count \'' + self.name + '\'!', 3000, 'orange darken-4');
            } else {
                Materialize.toast('Count \'' + self.name + '\' deleted!', 3000, 'blue-grey');
            }
        });

    },
    'click .cancel-delete'() {
        var self = this;
        $('.' + self._id + ' > .delete-count').toggle();
        $('.' + self._id + ' > .really-delete').toggle();
        $('.' + self._id + ' > .cancel-delete').toggle();
    },
});

Template.LinkDelete.helpers({
    pname: function () {
        const self = this;
        const p = Counts.findOne(self.primary);
        return p.name;
    },
    sname: function () {
        const self = this;
        const s = Counts.findOne(self.secondary);
        return s.name;
    },
    tname: function () {
        const self = this;
        if (self.type == 0) {
            return "reset";

        } else if (self.type == 1) {
            return "increase";

        } else if (self.type == 2) {
            return "decrease";

        }
    },
    ltype: function() {
        const self = this;
        if (self.type == 1 || self.type == 2) {
            return "every";
        } else {
            return "at";
        }
    }
})

Template.LinkDelete.events({
    'click .delete-link'(event) {
        var self = this;
        $('.' + self._id + ' > .delete-link').toggle();
        $('.' + self._id + ' > .really-delete').toggle();
        $('.' + self._id + ' > .cancel-delete').toggle();
    },
    'click .really-delete'() {
        var self = this;
        var projectId = FlowRouter.getParam('projectId');
        var linkId = self._id;
        Meteor.call('links.delete-from-all',linkId,projectId, function(error){
            if (error) {
                $('.' + linkId + ' > .delete-link').toggle();
                $('.' + linkId + ' > .really-delete').toggle();
                $('.' + linkId + ' > .cancel-delete').toggle();
                Materialize.toast('Failed to delete link!', 3000, 'orange darken-4');
            } else {
                Materialize.toast('Link deleted!', 3000, 'blue-grey');
            }
        });

    },
    'click .cancel-delete'() {
        var self = this;
        $('.' + self._id + ' > .delete-link').toggle();
        $('.' + self._id + ' > .really-delete').toggle();
        $('.' + self._id + ' > .cancel-delete').toggle();
    },


})



String.prototype.isEmpty = function() {
    return (this.length === 0 || !this.trim());
};

