/**
 * Created by milo on 25/01/2017.
 */

import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './list_projects.html';

Template.ListProjects.onCreated(function() {
    var self = this;
    self.autorun(function() {
        Meteor.subscribe('allprojects');
        ClientStorage.set('beecount_location', '/list');
    })
});


Template.ListProjects.helpers({
    projects() {
        return Projects.find({},{ sort: { sort_order: 1 } });
    },
    empty() {
        if (Projects.find({}).count() === 0) {
            return true;
        }
        return false;
    },
    isReady: function(sub) {
        if(sub) {
            return FlowRouter.subsReady(sub);
        } else {
            return FlowRouter.subsReady();
        }
    },
    sortOptions: function() {
        return {
            onSort: function(event) {
                console.log('Moved project #%d from %d to %d',
                    event.data.order, event.oldIndex, event.newIndex
                );
            }
        };
    }
});

Template.ListProjects.events({
    'click .delete'() {
        var self = this;
        $('.' + self._id + ' > .delete').toggle();
        $('.' + self._id + ' > .really-delete').toggle();
        $('.' + self._id + ' > .cancel-delete').toggle();
    },
    'click .really-delete'() {
        var self = this;
        Meteor.call('projects.remove', self._id, function(error){
            if (error) {
                $('.' + self._id + ' > .delete').toggle();
                $('.' + self._id + ' > .really-delete').toggle();
                $('.' + self._id + ' > .cancel-delete').toggle();
                Materialize.toast('Failed to delete project \'' + self.name + '\'!', 3000, 'orange darken-4');
            } else {
                Materialize.toast('Project \'' + self.name + '\' deleted!', 3000, 'blue-grey');
            }
        });

    },
    'click .cancel-delete'() {
        var self = this;
        $('.' + self._id + ' > .delete').toggle();
        $('.' + self._id + ' > .really-delete').toggle();
        $('.' + self._id + ' > .cancel-delete').toggle();
    },
    'click .open'() {
        var self = this;
        FlowRouter.go('/count/' + self._id)
    }
})


Template.Project.events({
    'click .move-up'() {
        var self = this;
        Meteor.call('projects.move-up',self._id);
    },
    'click .move-down'() {
        var self = this;
        Meteor.call('projects.move-down',self._id);
    },
    'click .sort-position'() {
        var self = this;
        Meteor.call('projects.reset-position',self._id);
    }
})
