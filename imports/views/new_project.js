/**
 * Created by milo on 25/01/2017.
 */

import { Meteor } from 'meteor/meteor';

import './new_project.html';

Template.NewProject.onCreated(function() {
    var self = this;
    self.autorun(function() {
        ClientStorage.set('beecount_location', 'new');
    })
})


Template.NewProject.events({
    'click #new-project'(event) {

        if (! Meteor.user()) {
            Materialize.toast('You need to log in before you can create projects.', 3000, 'orange darken-4');
            return;
        }

        // Get values manually from form element
        const name = $("#name").val();
        const notes = $("#notes").val();
        const counts = $.map($(".count-input"), function(count) {
            return count.value;
        });
        const count_notes = $.map($(".count-notes"), function(count) {
            return count.value || 'none';
        });

        // Perform validation
        var errors = [];
        if (!name) {
           errors.push("You'll need to give this project a name.");
        }
        if (counts.length == 0) {
            errors.push("You need to supply at least one count.")
        } else {
            var dodgy = false;
            counts.forEach(function(count){
                if (!count) {
                    dodgy = true;
                }
            })
            if (dodgy) {
               errors.push("At least one count needs naming. Please type a name, or delete any empty counts.");
            }
        }

        // Clear existing error area and render new errors, if
        $("#errors-go-here").empty();
        if (errors.length > 0) {
            Blaze.renderWithData(Template.ValidationErrors, {errors: errors}, $("#errors-go-here")[0])
            $(document).scrollTop( $("#errors-go-here").offset().top );
            return;
        }

        console.log("Counts: " + counts);
        console.log("Notes: " + count_notes);

        // Insert a project into the collection
        Meteor.call('projects.insert',name,notes,counts,count_notes,function(error){
            if (error) {
                console.log("Project creation error: " + error);
                Materialize.toast('Project \'' + name + '\' failed to save!', 3000, 'orange darken-4');
                return false;
            } else {
                Materialize.toast('Project \'' + name + '\' created!', 3000, 'blue-grey');
                FlowRouter.go('/list');
            }
        });


    },
    'click .add-count'(event) {
        Blaze.renderWithData(Template.NewCount, {}, $("#add-more-counts")[0])
        document.getElementById('delete-count').style.display = '';
    },
    'click .delete-count'(event) {
        const counts = $(".new-count").length;
        if ( counts > 0 ) {
            const element = $(".new-count")[counts - 1];
            element.parentNode.removeChild(element);
            Materialize.toast('Count deleted!', 500, 'blue-grey');
        }
        if ( counts == 1) {
            document.getElementById('delete-count').style.display = 'none';
        }
    }
});

String.prototype.isEmpty = function() {
    return (this.length === 0 || !this.trim());
};
