/**
 * Created by milo on 11/04/2017.
 */


import { Meteor } from 'meteor/meteor';

Template.Privacy.onCreated(function() {
    var self = this;
    self.autorun(function() {
        ClientStorage.set('beecount_location', '/privacy');
    })
})