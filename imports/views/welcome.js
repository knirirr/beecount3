/**
 * Created by milo on 11/04/2017.
 */


import { Meteor } from 'meteor/meteor';

Template.Welcome.onCreated(function() {
    var self = this;
    self.autorun(function() {
        ClientStorage.set('beecount_location', null);
    })
})

Template.Welcome.helpers({
    isMobile() {
        if (Meteor.isCordova) {
            return "<p>This is running as an Android app rather than a pure webapp.</p>";
        }
    }
})