/**
 * Created by milo on 13/02/2017.
 */

AccountsTemplates.configure({
    showForgotPasswordLink: true,
    enablePasswordChange: true,
    sendVerificationEmail: true,
    enforceEmailVerification: false,
    confirmPassword: true,
    showResendVerificationEmailLink: true,
    continuousValidation: true,

    // Privacy Policy and Terms of Use
    privacyUrl: 'privacy',
    //termsUrl: 'terms-of-use',

    // Display the form properly
    defaultLayoutType: 'blaze',
    defaultLayout: 'MainLayout',
    defaultLayoutRegions: { },
    defaultContentRegion: 'content'
});


AccountsTemplates.configureRoute('resetPwd');



/*
Meteor.AppCache.config({
    onlineOnly: [
        '/public/',
    ]
});
*/
