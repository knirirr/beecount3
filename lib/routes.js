/**
 * Created by milo on 24/01/2017.
 */

/*
 * Add footer: template in order to display breacrumbs in the template
 */

import { Meteor } from 'meteor/meteor';

import '../imports/api/projects.js';

FlowRouter.route(
    '/', {
        name: 'welcome',
        action()  {
            BlazeLayout.render('MainLayout', {content: 'Welcome'})
        }
    },
);

FlowRouter.route(
    '/instructions', {
        name: 'instructions',
        action()  {
            BlazeLayout.render('MainLayout', {content: 'Instructions'})
        }
    },
);

FlowRouter.route(
    '/privacy', {
        name: 'privacy',
        action()  {
            BlazeLayout.render('MainLayout', {content: 'Privacy'})
        }
    },
);

FlowRouter.route(
    '/new', {
        name: 'new',
        action()  {
            BlazeLayout.render('MainLayout', {content: 'NewProject' })
        }
    },
);

FlowRouter.route('/list', {
    name: 'list',
    action: function(params)  {
        BlazeLayout.render('MainLayout', {content: 'ListProjects', footer: 'MoveElements'})
    }
});

FlowRouter.route('/count/:projectId', {
    name: 'count',
    action: function(params) {
        BlazeLayout.render('MainLayout', {content: 'Counting', projectId: params.projectId, footer: 'MoveElements'})
    }
});

// This is to edit a project; counts should have separate edit pages
FlowRouter.route('/edit_project/:projectId', {
    name: 'edit_project',
    action: function(params) {
        BlazeLayout.render('MainLayout', {content: 'EditProject', projectId: params.projectId})
    }
});

FlowRouter.route('/edit_count/:countId', {
    name: 'edit_count',
    action: function(params) {
        BlazeLayout.render('MainLayout', {content: 'EditCount', countId: params.countId})
    }
});

FlowRouter.route('/account', {
    name: 'account',
    action: function(params)  {
        BlazeLayout.render('MainLayout', {content: 'Account'});
    }
});

/*
FlowRouter.route('/reset-password/:token', {
    name: 'accounts.reset-password',
    action(params) {
        AccountsTemplates.paramToken = params.token;
        console.log("GOT TOKEN: " + params.token);
        mount(SignupPage, {
            content: 'resetPassword',
        });
    },
});
*/