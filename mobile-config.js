/**
 * Created by milo on 04/04/2017.
 */

App.info({
    id: 'com.knirirr.beecount3',
    name: 'BeeCount 3',
    description: 'An awesome and flexible knitting counter',
    author: 'Knirirr',
    email: 'knirirr@gmail.com',
    website: 'https://knirirr.com/beecount',
    version: '0.0.1'
});

App.setPreference('Orientation', 'default');
App.setPreference('Orientation', 'all', 'ios');

//App.accessRule('https://beecountapp.knirirr.com', 'navigation');
//TODO: Check if Ravelry needed also

App.launchScreens({
    'iphone': 'icons/beecount_splash.png',
    'iphone_2x': 'icons/beecount_splash.png',
    'ipad_2x': 'icons/beecount_splash.png',
    'android': 'icons/beecount_splash.png',
    'android_mdpi': 'icons/beecount_splash.png',
    'android_hdpi': 'icons/beecount_splash.png',
    'android_xhdpi': 'icons/beecount_splash.png',
    'android_xxhdpi': 'icons/beecount_splash.png',
    // TODO: Produce a greater variety of these...
});

App.icons({
    'iphone_2x': 'icons/Icon-72@2x.png',
    'ipad': 'icons/Icon-72.png',
    'ipad_2x': 'icons/Icon-72@2x.png',
    'ios_spotlight': 'icons/Icon-40.png',
    'ios_spotlight_2x': 'icons/Icon-40@2x.png',
    'android_mdpi': 'icons/drawable-mdpi/ic_launcher.png',
    'android_hdpi': 'icons/drawable-hdpi/ic_launcher.png',
    'android_xhdpi': 'icons/drawable-xhdpi/ic_launcher.png',
    'android_xxhdpi': 'icons/drawable-xxhdpi/ic_launcher.png',
})

/*
iphone_2x (120x120)
iphone_3x (180x180)
ipad (76x76)
ipad_2x (152x152)
ipad_pro (167x167)
ios_settings (29x29)
ios_settings_2x (58x58)
ios_settings_3x (87x87)
ios_spotlight (40x40)
ios_spotlight_2x (80x80)
android_mdpi (48x48)
android_hdpi (72x72)
android_xhdpi (96x96)
android_xxhdpi (144x144)
android_xxxhdpi (192x192)
    */