import { Meteor } from 'meteor/meteor';


import '../imports/api/projects.js'
import '../imports/api/counts.js'
import '../imports/api/alerts.js'
import '../imports/api/links.js'
import '../imports/api/auth.js'


// Due to an annoying bug described here:
// https://github.com/meteor/meteor/issues/3506
import { ServiceConfiguration } from 'meteor/service-configuration';
ServiceConfiguration.configurations = new Mongo.Collection('meteor.loginServiceConfiguration');


//Sortable.collections = [Projects,Counts];

Meteor.publish('allprojects', function projectPublication() {
    this.autorun(function (computation) {
        let projects = Projects.find({knitter: this.userId});
        return projects;
    });
});
Meteor.publish('project-counts', function projectPublication(projectId) {
    check(projectId,String);
    this.autorun(function (computation) {
        let project = Projects.findOne({_id: projectId, knitter: this.userId});
        return Counts.find({_id: {$in: project.counts}});
    });
});
Meteor.publish('project-alerts',function alertPublication(projectId) {
    check(projectId,String);
    this.autorun(function (computation) {
        let project = Projects.findOne({_id: projectId, knitter: this.userId});
        return Alerts.find({_id: {$in: project.alerts}});
    });
});
Meteor.publish('project-links',function alertPublication(projectId) {
    check(projectId,String);
    this.autorun(function (computation) {
        let project = Projects.findOne({_id: projectId, knitter: this.userId});
        return Links.find({_id: {$in: project.links}});
        //return Links.find({});
    });
});
Meteor.publish('count-alerts',function alertPublication(countId) {
    check(countId,String);
    this.autorun(function (computation) {
        let count = Counts.findOne({_id: countId, knitter: this.userId});
        return Alerts.find({_id: {$in: count.alerts}});
    });
});
Meteor.publish('single-project', function projectPublication(projectId) {
    check(projectId,String);
    this.autorun(function (computation) {
        return Projects.find({_id: projectId, knitter: this.userId});
    });
});
Meteor.publish('single-count', function countPublication(countId) {
    check(countId,String);
    this.autorun(function (computation) {
        return Counts.find({_id: countId, knitter: this.userId});
    });
});
Meteor.publish('project-from-count',function projectPublication(countId) {
    check(countId,String);
    this.autorun(function (computation) {
        return Projects.find({},{counts: countId, knitter: this.userId});
    });
});
Meteor.publish('allusers', function accountPublication() {
    this.autorun(function (computation) {
        if (Meteor.call('auth.is_admin')) {
            return Meteor.users.find({});
        } else {
            return Meteor.users.find({_id: this.userId});
        }


    })
});

Meteor.startup(() => {
    Migrations.migrateTo('latest');
    // Apparently of no use.
    /*
    Accounts.urls.resetPassword = (token) => {
        return Meteor.absoluteUrl('reset-password/' + token);
    };
    */
});


Migrations.add({
    version: 1,
    name: 'Add alerts array to counts and projects',
    up: function() {
        Counts.find().forEach(function (count) {
            if (!count.alerts) {
               Counts.update(count._id, {$set: {alerts: []}});
            }
        });
        Projects.find().forEach(function (project) {
            if (!project.alerts) {
               Projects.update(project._id, {$set: {alerts: []}});
            }
        });
    }
});

Migrations.add({
    version: 2,
    name: 'Add links array to counts and projects',
    up: function() {
        Projects.find().forEach(function (project) {
            if (!project.links) {
               Projects.update(project._id, {$set: {links: []}});
            }
        });
        Counts.find().forEach(function (count) {
            if (!count.links) {
               count.update(count._id, {$set: {links: []}});
            }
        });
    }
});

Migrations.add({
    version: 3,
    name: 'Add alreadyVisited array to projects',
    up: function() {
        Projects.find().forEach(function (project) {
            if (!project.alreadyVisited) {
               Projects.update(project._id, {$set: {alreadyVisited: []}});
            }
        });
    }
});

