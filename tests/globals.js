/**
 * Created by milo on 06/04/2017.
 */

module.exports = {
    before: function(done){
        require('dotenv').config();
        done();
    }
}

/*
 * The .env file must go in tests/
 * EMAIL=me@my.domain
 * PASSWORD=my_awesome_password
 */