module.exports = {

    before(client) {
        client.url('http://localhost:3000').pause(1000);

        // Login form
        client.expect.element('#at-field-email').to.be.an('input');
        client.expect.element('#at-field-password').to.be.an('input');
        client.setValue('#at-field-email', process.env.EMAIL);
        client.setValue('#at-field-password', process.env.PASSWORD);
        client.submitForm('#at-pwd-form').pause(1000);

    },
    after(client) {
        client.end();
    },

    'Welcome test' : function (client) {

        // Page load with main title
        client.expect.element('body').to.be.present.before(1000);
        client.expect.element('#index-banner').text.to.contain('BeeCount');


        // Deploy to project list
        client.url('http://localhost:3000/list').pause(1000);
        client.expect.element('.header').text.to.contain('Projects');

    },

    'Counting test' : function (client) {
        client.url('http://localhost:3000/list').pause(1000);
        client.getText('.card-title', function(result) {
            this.assert.equal(typeof result, "object");
            client.click('.open').pause(1000);
            client.expect.element('.header').text.to.contain(result.value);

        });



    }

};
