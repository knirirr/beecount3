/**
 * Created by milo on 06/04/2017.
 */

function countProjects() {
    browser.waitForExist('.single-project',2000);
    const elements = browser.elements('.single-project');
    return elements.value.length;
};


describe('projects', function () {
    /*
    beforeEach(function () {
        browser.url('http://localhost:3000/list');
    });
    */

    it('can create a project @watch', function () {
        browser.url('http://localhost:3000/list');
        const initialCount = countProjects();
        var result = server.apply('projects.insert',[
            'Test Project',
            'Exciting notes...',
            ['one','two'],
            ['note one','note two']
        ])
        assert.equal(countProjects(), initialCount + 1);
    });
});

// stuff